export const sqrt = Math.sqrt;
export const pi   = Math.PI;
export const pow   = Math.pow;

/**
 Geron expression to evaluate triangle square
 */
export function getTriangleSquare(a, b, c) {
    let p = 0.5 * (a + b + c);

    return sqrt(p * (p-a) * (p-b) * (p-c));
}

/**
 * calculate circle square
 * @param r - radius
 * @returns {number} circule square
 */
export function getCircleSquare(r) {
    return  pi * pow(r, 2);
}

/**
 * calculate rectangular parallelepiped(box) square
 * @param w - width
 * @param l - length
 * @param h - height
 * @returns {number} volume of rectangular parallelepiped(box)
 */
export function getParallelepipedVolume(w, l, h) {
    return  w * l * h;
}