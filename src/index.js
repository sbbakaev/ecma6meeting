import express from 'express';
import {app} from './init';
import {getTriangleSquare, getCircleSquare, getParallelepipedVolume} from './geomethry';


// Here routes defined
import './routes';
app.listen(3000, () => {
    (function () {
        'use strict';

        class Shape {

            constructor(title, area) {
                this.title = title;
                this.area = area;
            }

            // The way to determine abstract class
            /*constructor() {
                if(new.target == Shape) {
                    throw new TypeError("Cannot construct Shape instances directly");
                }
            }*/

            // The way to determine abstract method in class
            /*constructor() {
                if(this.getArea === undefined) {
                    throw new TypeError("getArea is abstract and must be overridden");
                }
            }*/

            getTitle() {
                return this.title;
            }

            getArea() {
                return this.area;
            }

        }

        class Triangle extends Shape {

            constructor(a, b, c) {
                /*super('This is a triangle', () => {
                 // Geron expression should be here
                 });*/

                super('This is a triangle', getTriangleSquare(/* new language feature */...[a, b, c]));

                this.a = a;
                this.b = b;
                this.c = c;
            }

        }

        class Circle extends Shape {
            constructor(r) {
                super('This is a circle: ', getCircleSquare(r));
                this.r = r;
            }
        }

        class Parallelepiped extends Shape {
            constructor(w, l, h) {
                super('This is a parallelepiped: ', getParallelepipedVolume(w, l, h));
                this.w = w;
                this.l = l;
                this.h = h;
            }
        }



        // Define a triangle with 90* angle
        var directTriangle       = new Triangle(3, 4, 5);
        var directParallelepiped = new Parallelepiped(3, 4, 5);
        var directCircle         = new Circle(1);

        console.log(directCircle.getTitle()+': '+ directCircle.getArea());
        console.log(directParallelepiped.getTitle()+': '+ directParallelepiped.getArea());
        console.log(directTriangle.getTitle()+': '+ directTriangle.getArea());

    }())
});